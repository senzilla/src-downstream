# SPDX-License-Identifier: ISC

UPSTREAM_MAIN=	sz-7.2
RELEASE_SOURCE=	build

.include <sz.own.mk>
.include <sz.downstream.mk>
